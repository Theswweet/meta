#include <iostream>
using namespace std;

int main()
{
	int num;
	cout << "Enter the force of the wind: ";
	cin >> num;

	if (num < 1)
	{
		cout << "This is a light, calm breeze.";
	}
	else if (num >= 1, num < 4)
	{
		cout << "This is a slightly less calm, breeze.";
	}
	else if (num >= 4, num < 28)
	{
		cout << "This is a breeze.";
	}
	else if (num >= 28, num < 48)
	{
		cout << "This is a Gale.";
	}
	else if (num >= 48, num < 63)
	{
		cout << "This is a storm.";
	}
	else if (num >= 63)
	{
		cout << "This is a storm.";
	}

	return 0;
}
